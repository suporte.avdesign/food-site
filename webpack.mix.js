const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/contact-app.js', 'public/assets/app/js');
mix.js('resources/js/plans-app.js', 'public/assets/app/js')
    .sass('resources/sass/app.scss', 'public/css');
