<?php

use Illuminate\Support\Facades\Route;

//Lista os assuntos para contato do site
Route::apiResource('subjects', 'Api\\SubjectContactController');
//Grava o contato do site no bd
Route::apiResource('contact', 'Api\\ContactController');
Route::apiResource('plans', 'Api\\PlansController');
