<?php
// Page/Home
Route::get('/', 'Web\HomeController@index')->name('home');
// Page/Plano: Opção do usuário
Route::get('assinar-plano/{slug}', 'Web\PlansController@subscription')->name('plan.subscription');
// Page/Contact - Form (vue.js)
Route::get('contato', 'Web\ContactController@index')->name('contact');

//Auth::routes();
