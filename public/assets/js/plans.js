(function ($) {
    "use strict";

    jQuery(document).ready(function ($) {
        $(document).on('submit', '#plans_form_submit', function (e) {
            e.preventDefault();
            var name = $('#name').val();
            var email = $('#email').val();
            var subject = $('#subject').val();
            var surename = $('#surename').val();
            var message = $('#message').val();
            var phone = $('#phone').val();

            if (name && email && phone && subject && message && surename) {
                $.ajax({
                    type: "POST",
                    url: 'contact',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'name': name,
                        'email': email,
                        'subject': subject,
                        'surename': surename,
                        'message': message,
                        'phone': phone,
                    },
                    success: function (data) {
                        if (data.success == true) {
                            $('#plans_form_submit').children('.email-success').remove();
                            $('#plans_form_submit').prepend('<span class="alert alert-success email-success">' + data.message + '</span>');
                            $('#name').val('');
                            $('#email').val('');
                            $('#message').val('');
                            $('#surename').val('');
                            $('#subject').val('');
                            $('#phone').val('');
                            // $('#map').height('576px');
                            $('.email-success').fadeOut(3000);
                        }
                    },
                    error: function (res) {

                    }
                });
            } else {
                $('#plans_form_submit').children('.email-success').remove();
                $('#plans_form_submit').prepend('<span class="alert alert-danger email-success">All Fields are Required.</span>');
                // $('#map').height('576px');
                $('.email-success').fadeOut(3000);
            }

        });
    })

}(jQuery));