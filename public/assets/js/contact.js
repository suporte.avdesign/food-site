(function ($) {
    "use strict";


    jQuery(document).ready(function ($) {
        $(document).on('submit', '#contact_form_submit', function (e) {
            e.preventDefault();
            $('#return-submit').html('');
            var company = $('#company').val();
            var name = $('#name').val();
            var email = $('#email').val();
            var subject = $('#subject').val();
            var message = $('#message').val();
            var phone = $('#phone').val();
                $.ajax({
                    type: "POST",
                    url: 'contact',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'name': name,
                        'email': email,
                        'subject': subject,
                        'company': company,
                        'message': message,
                        'phone': phone,
                    },
                    success: function (data) {
                        if (data.success == true) {
                            $('#name').val('');
                            $('#email').val('');
                            $('#message').val('');
                            $('#company').val('');
                            $('#subject').val('');
                            $('#phone').val('');
                        }

                        $('#return-submit').prepend(alertMessage('success', data.message));
                        $('#return-submit').fadeOut(6000);
                    },
                    error: function (res) {

                    }
                });

        });
    })

}(jQuery));

function alertMessage(cls, msg) {
    return '<div class="alert alert-'+ cls +'" role="alert">'+ msg +'</div>';
};
