<?php

namespace App\Http\Controllers\Web;

use App\Models\Plan;
use App\Services\PlanService;
use App\Services\CompanyService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\PlanRequest;

use Illuminate\Support\Facades\Session;

class PlansController extends Controller
{
    private $view = 'pages.plans';
    private $plan;
    private $service;
    
    public function __construct(Plan $plan, PlanService $service)
    {
        $this->plan = $plan;
        $this->service = $service;
    }

    /**
     * Plano escolhido pelo visitante.
     * Cria Session de um plano específico.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function subscription($slug)
    {
        if (!$plan = $this->plan->whereSlug($slug)->first()) {
            abort(404, 'Página não encontrada');
        }
        
        Session::put('plan', $plan);
        return view("{$this->view}.subscription");
    
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanRequest $request)
    {
        $data = $request->except('_token');
        dd($data);
        if (!$plan = session('plan') ) {
            return redirect()->back();
        }
        
        $companyService = app(CompanyService::class);
        $user = $companyService->make($plan, $data);
        
        
        return $user;
    }
}
