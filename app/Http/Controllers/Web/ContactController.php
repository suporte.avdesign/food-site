<?php

namespace App\Http\Controllers\Web;

use App\Models\Contact;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    private $view = 'pages.contact';
    private $contact;
    
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("{$this->view}.contact-1");
    }
    
}
