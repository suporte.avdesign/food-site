<?php

namespace App\Http\Controllers\Api;

use App\Models\Plan;
use App\Services\CompanyService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\PlanRequest;

class PlansController extends Controller
{
    private $plan;
    
    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanRequest $request)
    {
        $plan = $this->plan->find(98396966);
        $companyService = app(CompanyService::class);
        $user = $companyService->make($plan, $request->validated());
        
    
        dd($user);
    }
}
