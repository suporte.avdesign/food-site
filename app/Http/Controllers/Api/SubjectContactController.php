<?php

namespace App\Http\Controllers\Api;

use App\Models\SubjectContact;
use App\Http\Controllers\Controller;
use App\Http\Resources\SubjectContactResource;

class SubjectContactController extends Controller
{
    /**
     * Prencher o select (id,title) dos assuntos do contato.
     */
    public function index()
    {
        return SubjectContactResource::collection(SubjectContact::all());
    }
}
