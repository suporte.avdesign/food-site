<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['company'] = 'required|string|min:3|unique:companies,name';
        $rules['name'] = 'required|string|min:3';
        $rules['email'] = 'required|string|email|unique:users';
        $rules['phone'] = 'required|string|min:14';
        $rules['document'] = 'required|string|min:14|unique:companies';
        $rules['password'] = 'required|string|min:6|confirmed';
        
        return $rules;
    
    }
    
    public function messages()
    {
        $message = [
            'company.required' => 'O nome da Empresa é obrigatório.',
            'company.min' => 'O nome da Empresa deverá conter no mínimo 3 caracteres.',
            'document.unique' => 'O nome da Empresa já se encontra utilizado.',
            'name.required' => 'O nome é obrigatório.',
            'name.min' => 'O nome deverá conter no mínimo 3 caracteres.',
            'email.required' => 'O email é obrigatório.',
            'email.email' => 'Digite um email válido.',
            'email.unique' => 'Este email já se encontra utilizado.',
            'phone.required' => 'O telefone é obrigatório.',
            'phone.min' => 'O telefone deverá conter no mínimo 14 caracteres.',
            'document.required' => 'O CNPJ é obrigatório.',
            'document.min' => 'O CNPJ deverá conter no mínimo 14 caracteres.',
            'document.unique' => 'Este CNPJ já se encontra utilizado.',
            'password.required' => 'A senha é obrigatória.',
            'password.confirmed' => 'As senhas não são iguais.',

        ];
        
        return $message;
    }
}
