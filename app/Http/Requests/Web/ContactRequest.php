<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['company'] = ['required', 'min:3'];
        $rules['name'] = ['required', 'min:3'];
        $rules['phone'] = ['required', 'min:14'];
        $rules['email'] = ['required', 'email'];
        $rules['message'] = ['required', 'min:3'];
        $rules['subject_contact_id']  = ['required'];
        
        return $rules;
    }
    
    public function messages()
    {
        $message = [
            'company.required' => 'O nome da Empresa é obrigatório.',
            'company.min' => 'O nome da Empresa deverá conter no mínimo 3 caracteres.',
            'name.required' => 'O nome é obrigatório.',
            'name.min' => 'O nome deverá conter no mínimo 3 caracteres.',
            'phone.required' => 'O telefone é obrigatório.',
            'phone.min' => 'O nome telefone conter no mínimo 14 caracteres.',
            'email.required' => 'O email é obrigatório.',
            'email.email' => 'Digite um email válido.',
            'subject_contact_id.required' => 'O assunto é obrigatório.',
            'message.required' => 'A mensagem é obrigatório.',
            'message.min' => 'A mensagem deverá conter no mínimo 3 caracteres.',
        ];
        
        return $message;
    }
}
