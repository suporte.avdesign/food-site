<?php
    
    
namespace App\Services\Traits;


trait ConfigSiteTrait
{
    public function getConfig()
    {
        $config = new \stdClass();
        $config->url_register = "http://starter-kit.test/register";
        
        return $config;
    }
}