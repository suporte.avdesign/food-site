<?php
    
    
namespace App\Services;

use App\Models\Plan;
use Illuminate\Support\Str;

class CompanyService
{
    private $plan;
    private $data = [];
    
    /**
     * Chama os métodos store e retorna usuário cadastrado.
     *
     *
     * @param Plan $plan
     * @param array $data
     * @return mixed
     */
    public function make(Plan $plan, array $data)
    {
        $this->plan = $plan;
        $this->data = $data;
        
        $company = $this->storeCompany();
        
        $user = $this->storeUser($company);
        
        return $user;
    }
    
    /**
     * Grava os dados da Empresa.
     * @return mixed
     */
    public function storeCompany()
    {
        return $this->plan->companies()->create([
            'uuid' => Str::uuid(),
            'name' => $this->data['company'],
            'email' => strtolower($this->data['email']),
            'phone' => $this->data['phone'],
            'document' => $this->data['document'],
            'slug' => Str::kebab($this->data['company']),
            //Plano Free expires.
            'subscription' => now(),
            'expires_at' => now()->addDays(7),
        ]);
    }
    
    /**
     * Grava os dados do Usuário.
     * @param $company
     * @return mixed
     */
    public function storeUser($company)
    {
        $user = $company->users()->create([
            'name' => $this->data['name'],
            'email' => strtolower($this->data['email']),
            'password' => bcrypt($this->data['password']),
        ]);
        
        return $user;
    }

}