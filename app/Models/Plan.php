<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['name', 'slug', 'price', 'description', 'order', 'active'];
    
    
    /**
     * Retorna os detalhes de um plano específico.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->hasMany(PlanDetail::class);
    }
    
    /**
     * Retorna todos as Empresas vinculadas ao plano.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companies()
    {
        return $this->hasMany(Company::class);
    }
    
}
