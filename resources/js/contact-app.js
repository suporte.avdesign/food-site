require('./bootstrap');

window.Vue = require('vue');

Vue.component('contact-submit', require('./components/ContactSubmit.vue').default);

const app = new Vue({
    el: '#appContact',
});