require('./bootstrap');

window.Vue = require('vue');

Vue.component('plans-submit', require('./components/PlanSubscription.vue').default);

const app = new Vue({
    el: '#appPlans',
});