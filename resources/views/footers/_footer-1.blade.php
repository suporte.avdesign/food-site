<footer class="footer-section bg_img" data-background="{{asset('assets/images/footer/footer-bg.jpg')}}">
    <div class="container">
        <div class="footer-top padding-top padding-bottom">
            <div class="logo">
                <a href="#0">
                    <img src="{{asset('assets/images/logo/footer-logo.png')}}" alt="logo">
                </a>
            </div>
            <ul class="social-icons">
                <li>
                    <a href="#0"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li>
                    <a href="#0" class="active"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-pinterest-p"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-google-plus-g"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fab fa-instagram"></i></a>
                </li>
            </ul>
        </div>
        <div class="footer-bottom">
            <ul class="footer-link">
                <li>
                    <a href="#0">About</a>
                </li>
                <li>
                    <a href="#0">FAQs</a>
                </li>
                <li>
                    <a href="#0">Contact</a>
                </li>
                <li>
                    <a href="#0">Terms of Service</a>
                </li>
                <li>
                    <a href="#0">Privacy</a>
                </li>
            </ul>
        </div>
        <div class="copyright">
            <p>
                Copyright © 2020.All Rights Reserved By <a href="#0">Mosto</a>
            </p>
        </div>
    </div>
</footer>
