<header class="header-section">
    <div class="container">
        <div class="header-wrapper">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('assets/images/logo/logo.png')}}" alt="logo">
                </a>
            </div>
            <ul class="menu">
                <li>
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li>
                    <a href="#0">Feature</a>
                    <ul class="submenu">
                        <li>
                            <a href="feature.html">Feature 1</a>
                        </li>
                        <li>
                            <a href="feature-2.html">Feature 2</a>
                        </li>
                        <li>
                            <a href="feature-3.html">Feature 3</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="pricing-plan.html">Pricing</a>
                </li>
                <li>
                    <a href="#0">Pages</a>
                    <ul class="submenu">
                        <li>
                            <a href="about.html">about</a>
                        </li>
                        <li>
                            <a href="app-download.html">app download</a>
                        </li>
                        <li>
                            <a href="#0">Team</a>
                            <ul class="submenu">
                                <li>
                                    <a href="team.html">Team</a>
                                </li>
                                <li>
                                    <a href="team-single.html">Team Single</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#0">Account</a>
                            <ul class="submenu">
                                <li>
                                    <a href="sign-up.html">Sign Up</a>
                                </li>
                                <li>
                                    <a href="sign-in.html">Sign In</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="faqs.html">FAQs</a>
                        </li>
                        <li>
                            <a href="partners.html">Partners</a>
                        </li>
                        <li>
                            <a href="privacy-policy.html">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="coming-soon.html">Coming Soon</a>
                        </li>
                        <li>
                            <a href="change-password.html">Change Password</a>
                        </li>
                        <li>
                            <a href="reset-password.html">Password Reset</a>
                        </li>
                        <li>
                            <a href="reviews.html">review</a>
                        </li>
                        <li>
                            <a href="404.html">404</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#0">Blog</a>
                    <ul class="submenu">
                        <li>
                            <a href="blog.html">blog style 1</a>
                        </li>
                        <li>
                            <a href="blog-type-two.html">blog style 1</a>
                        </li>
                        <li>
                            <a href="blog-single-1.html">blog Single 1</a>
                        </li>
                        <li>
                            <a href="blog-single-2.html">blog Single 2</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('contact')}}">Contato</a>
                </li>
                <li class="d-sm-none">
                    <a href="#0" class="m-0 header-button">Get Mosto</a>
                </li>
            </ul>
            <div class="header-bar d-lg-none">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="header-right">
                <select class="select-bar">
                    <option value="en">En</option>
                    <option value="Bn">Bn</option>
                    <option value="pk">Pk</option>
                    <option value="Fr">Fr</option>
                </select>
                <a href="javascript:void(0);"  class="header-button d-none d-sm-inline-block">Veja Mais</a>
            </div>
        </div>
    </div>
</header>