@extends('layouts.web')
@push('title')
    <title>Home </title>
@endpush
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/light/css/main.css')}}">
@endpush

@section('content')
    {{--============= Banners Topo =============--}}
    @include('banners._banner-1')
    {{--============= Serviços  =============--}}
    @include('contents.colaborations._slider-1')
    {{--============= Planos e Preços =============--}}
    @include('contents.plans.pricing-1')
    {{--============= Depoimentos dos Clientes =============--}}
    @include('contents.testimonials.testimonial-1')
@endsection