@extends('layouts.error')
@push('title')
    <title>{{$exception->getMessage()}}</title>
@endpush
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/light/css/main.css')}}">
@endpush

@section('content')
    <!--============= Error In Section Starts Here =============-->
    <div class="error-section bg_img" data-background="{{asset('assets/images/404/404-bg.jpg')}}">
        <div class="container">
            <div class="man1">
                <img src="{{asset('assets/images/404/man_01.png')}}" alt="404" class="wow bounceInUp" data-wow-duration=".5s" data-wow-delay=".5s">
            </div>
            <div class="man2">
                <img src="{{asset('assets/images/404/man_02.png')}}" alt="404" class="wow bounceInUp" data-wow-duration=".5s">
            </div>
            <div class="error-wrapper wow bounceInDown" data-wow-duration=".7s" data-wow-delay="1s">
                <h1 class="title">404</h1>
                <h3 class="subtitle">{{$exception->getMessage()}}</h3>
                <a href="javascript:history.back()" class="button-5">Voltar</a>
            </div>
        </div>
    </div>

@endsection
