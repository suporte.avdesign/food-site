<section class="banner-1 bg_img oh" data-background="{{asset('assets/images/banner/banner-bg-1.jpg')}}">

    <div class="dot-1 d-none d-lg-block">
        <img src="{{asset('assets/images/banner/dot-big.png')}}" alt="banner">
    </div>
    <div class="dot-2 d-none d-lg-block">
        <img src="{{asset('assets/images/banner/dot-big.png')}}" alt="banner">
    </div>
    <div class="dot-3">
        <img src="{{asset('assets/images/banner/dot-sm.png')}}" alt="banner">
    </div>
    <div class="dot-4">
        <img src="{{asset('assets/images/banner/dot-sm.png')}}" alt="banner">
    </div>
    <div class="banner-1-shape d-none d-lg-block">
        <img src="{{asset('assets/css/img/banner1-shape.png')}}" alt="css">
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-content-1 cl-white">
                    <h1 class="title">Perfect App for more than just fun</h1>
                    <p>
                        The simple, intuitive and powerful app to manage your work.
                    </p>
                    <div class="banner-button-group">
                        <a href="#0" class="button-4">Start Using for Free</a>
                        <a href="#0" class="button-4 active">Explore Features</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="banner-1-slider-wrapper">
                    <div class="banner-1-slider owl-carousel owl-theme">
                        <div class="banner-thumb">
                            <img src="{{asset('assets/images/banner/banner1-1.png')}}" alt="banner">
                        </div>
                        <div class="banner-thumb">
                            <img src="{{asset('assets/images/banner/banner1-2.png')}}" alt="banner">
                        </div>
                        <div class="banner-thumb">
                            <img src="{{asset('assets/images/banner/banner1-3.png')}}" alt="banner">
                        </div>
                        <div class="banner-thumb">
                            <img src="{{asset('assets/images/banner/banner1-1.png')}}" alt="banner">
                        </div>
                        <div class="banner-thumb">
                            <img src="{{asset('assets/images/banner/banner1-2.png')}}" alt="banner">
                        </div>
                        <div class="banner-thumb">
                            <img src="{{asset('assets/images/banner/banner1-3.png')}}" alt="banner">
                        </div>
                    </div>
                    <div class="ban-click">
                        <div class="thumb">
                            <img src="{{asset('assets/images/banner/click.png')}}" alt="banner">
                        </div>
                        <span class="cl-white">Click Me</span>
                    </div>
                    <div class="arrow">
                        <img src="{{asset('assets/images/banner/arrow.png')}}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>