<section class="banner-8 oh">
    <div class="banner-shape-8 d-lg-block d-none">
        <img src="{{asset('assets/images/banner/banner-shape-8.png')}}" alt="banner">
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-content-8">
                    <h1 class="title">Enlarge Your Business With Web app</h1>
                    <p>
                        The simple, intuitive and powerful app to manage your work.
                    </p>
                    <div class="banner-button-group">
                        <a href="#0" class="button-4">Start Using for Free</a>
                        <a href="#0" class="button-4 active">Explore Features</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 d-none">
            </div>
            <div class="col-12">
                <div class="counter-wrapper-3">
                    <div class="counter-item">
                        <div class="counter-thumb">
                            <img src="{{asset('assets/images/icon/counter1.png')}}" alt="icon">
                        </div>
                        <div class="counter-content">
                            <h2 class="title"><span class="counter">17501</span></h2>
                            <span class="name">Premium User</span>
                        </div>
                    </div>
                    <div class="counter-item">
                        <div class="counter-thumb">
                            <img src="{{asset('assets/images/icon/counter2.png')}}" alt="icon">
                        </div>
                        <div class="counter-content">
                            <h2 class="title"><span class="counter">1987</span></h2>
                            <span class="name">Daily Visitors</span>
                        </div>
                    </div>
                    <div class="counter-item">
                        <div class="counter-thumb">
                            <img src="{{asset('assets/images/icon/counter5.png')}}" alt="icon">
                        </div>
                        <div class="counter-content">
                            <h2 class="title"><span class="counter">95</span><span>%</span></h2>
                            <span class="name">Satisfaction</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>