<section class="testimonial-section padding-top pt-lg-half padding-bottom pos-rel oh">
    <div class="container">
        <div class="ball-3 style2 d-none d-lg-block" data-paroller-factor="0.30" data-paroller-factor-lg="-0.30" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="{{asset('assets/images/client/circle2.png')}}" alt="client">
        </div>
        <div class="ball-6 style2 d-none d-lg-block" data-paroller-factor="-0.30" data-paroller-factor-lg="0.60" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="{{asset('assets/images/client/circle1.png')}}" alt="client">
        </div>
        <div class="row justify-content-between flex-wrap-reverse align-items-center">
            <div class="col-lg-7">
                <div class="testimonial-wrapper style-two">
                    <a href="#0" class="testi-next trigger">
                        <img src="{{asset('assets/images/client/left.png')}}" alt="client">
                    </a>
                    <a href="#0" class="testi-prev trigger">
                        <img src="{{asset('assets/images/client/right.png')}}" alt="client">
                    </a>
                    <div class="testimonial-area testimonial-slider owl-carousel owl-theme">
                        <div class="testimonial-item">
                            <div class="testimonial-thumb">
                                <div class="thumb">
                                    <img src="{{asset('assets/images/client/client1.jpg')}}" alt="client">
                                </div>
                            </div>
                            <div class="testimonial-content">
                                <div class="ratings">
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                </div>
                                <p>Produto incrível</p>
                                <p>Os caras colocaram um grande esforço neste aplicativo e se concentraram na simplicidade e facilidade de uso</p>
                                {{-- <h5 class="title"><a href="#0">Deixe um comentário</a></h5> --}}
                            </div>
                        </div>
                        <div class="testimonial-item">
                            <div class="testimonial-thumb">
                                <div class="thumb">
                                    <img src="{{asset('assets/images/client/client1.jpg')}}" alt="client">
                                </div>
                            </div>
                            <div class="testimonial-content">
                                <div class="ratings">
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                </div>
                                <p>Produto incrível</p>
                                <p>Os caras colocaram um grande esforço neste aplicativo e se concentraram na simplicidade e facilidade de uso</p>
                                {{-- <h5 class="title"><a href="#0">Deixe um comentário</a></h5> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="testi-wrapper">
                    <div class="testi-header">
                        <div class="section-header left-style">
                            <h5 class="cate">Depoimentos</h5>
                            <h2 class="title">Mais de 5000 clientes satisfeitos</h2>
                        </div>
                        <a href="#0" class="button-3 active">Deixe um comentário<i class="flaticon-right"></i></a>
                    </div>
                    <div class="sponsor-slider-two owl-theme owl-carousel">
                        <div class="sponsor-thumb">
                            <img src="{{asset('assets/images/sponsor/sponsor1.png')}}" alt="sponsor">
                        </div>
                        <div class="sponsor-thumb">
                            <img src="{{asset('assets/images/sponsor/sponsor2.png')}}" alt="sponsor">
                        </div>
                        <div class="sponsor-thumb">
                            <img src="{{asset('assets/images/sponsor/sponsor3.png')}}" alt="sponsor">
                        </div>
                        <div class="sponsor-thumb">
                            <img src="{{asset('assets/images/sponsor/sponsor4.png')}}" alt="sponsor">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
