<section class="colaboration-section padding-top-2 padding-bottom-2 oh" id="screenshot">
    <div class="container">
        <div class="row align-items-center flex-wrap-reverse">
            <div class="col-lg-6 col-xl-7 rtl">
                <div class="collaboration-anime-area">
                    <div class="main-thumb">
                        <img src="{{asset('assets/images/collaboration/main.png')}}" alt="colaboration" />
                    </div>
                    <div class="mobile wow slideInUp" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: slideInUp;">
                        <div class="show-up">
                            <img src="{{asset('assets/images/collaboration/mobile.png')}}" alt="colaboration" />
                        </div>
                        <div class="mobile-slider owl-theme owl-carousel ltr owl-rtl owl-loaded">
                            <div class="owl-stage-outer">
                                <div class="owl-stage" style="transform: translate3d(688px, 0px, 0px); transition: all 0.25s ease 0s; width: 1376px;">
                                    <div class="owl-item cloned" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen3.png')}}" style="background-image: url('{{asset('assets/images/collaboration/screen3.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item cloned" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen4.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen4.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen1.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen1.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen2.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen2.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item active" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen3.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen3.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen4.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen4.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item cloned" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen1.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen1.png')}}'');"></div>
                                    </div>
                                    <div class="owl-item cloned" style="width: 172px;">
                                        <div class="mobile-item bg_img" data-background="{{asset('assets/images/collaboration/screen2.png')}}'" style="background-image: url('{{asset('assets/images/collaboration/screen2.png')}}'');"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-nav disabled">
                                <div class="owl-prev">prev</div>
                                <div class="owl-next">next</div>
                            </div>
                            <div class="owl-dots disabled"></div>
                            <div class="owl-thumbs"></div>
                        </div>
                    </div>
                    <div class="girl wow slideInLeft" style="visibility: visible; animation-name: slideInLeft;">
                        <img src="{{asset('assets/images/collaboration/girl.png')}}'" alt="colaboration" />
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-5">
                <div class="section-header left-style">
                    <h5 class="cate">Intuitive interface for collaboration</h5>
                    <h2 class="title">Easy &amp; Perfect Solution</h2>
                    <p>
                        Numerous features make it possible to customize the system in accordance with all your needs.
                    </p>
                </div>
                <div class="colaboration-wrapper">
                    <div class="colaboration-slider owl-carousel owl-theme owl-rtl owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="transform: translate3d(1380px, 0px, 0px); transition: all 0.25s ease 0s; width: 2760px;">
                                <div class="owl-item cloned" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 345px;">
                                    <div class="colaboration-item">
                                        <div class="colaboration-thumb">
                                            <div class="icon">
                                                <i class="flaticon-data-management"></i>
                                            </div>
                                        </div>
                                        <div class="colaboration-content">
                                            <h4 class="title">Easy to Manage Your All Data</h4>
                                            <p>
                                                The satisfaction of users is the most important and the focus is on usability and completeness
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-nav disabled">
                            <div class="owl-prev">prev</div>
                            <div class="owl-next">next</div>
                        </div>
                        <div class="owl-dots disabled"></div>
                        <div class="owl-thumbs"></div>
                    </div>
                    <div class="cola-nav">
                        <a href="#0" class="cola-prev mr-4">
                            <img src="{{asset('assets/images/collaboration/left.png')}}" alt="colaboration" />
                        </a>
                        <a href="#0" class="cola-next">
                            <img src="{{asset('assets/images/collaboration/right.png')}}" alt="colaboration" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
