<section class="pricing-section padding-top padding-bottom pos-rel oh" id="pricing">
    <div class="extra-bg bg_img bottom_center" data-background="{{assert('assets/images/pricing/pricing-bg.png')}}"></div>
    <div class="container">
        <div class="section-header pb-30">
            <h3 class="cate">Escolha o plano certo para você</h3>
            <p>A ShopZap tem planos, que vão ao encontro das suas necessidades. Inscreva-se em um plano adequado ao tamanho de sua empresa.</p>
        </div>
        <div class="pricing-wrapper-4 mb-30-none">
            @foreach($plans as $plan)
                <div class="pricing-item-4">
                    <div class="pricing-header">
                        <h2 class="title"><sup>R$</sup>{{number_format($plan->price, '2', ',', '.')}}</h2>
                        <span>15 dias grátis </span>
                    </div>
                    <div class="pricing-body">
                        <h4 class="info">{{$plan->name}}</h4>
                        <ul>
                            @foreach($plan->details as $detail)
                                <li>{{$detail->resource}}</li>
                            @endforeach
                        </ul>
                        <a href="{{route('plan.subscription', $plan->slug)}}" class="button-3 long-light">Selecione Plano <i class="flaticon-right"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
