@extends('layouts.web')
@push('title')
    <title>Fale Conosco</title>
@endpush
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/light/css/main.css')}}">
@endpush
@section('content')
<div id="appContact">
    <!--============= Header Section Ends Here =============-->
    <section class="page-header single-header bg_img oh" data-background="{{asset('assets/images/page-header.png')}}">
        <div class="bottom-shape d-none d-md-block">
            <img src="{{asset('assets/images/page-header.png')}}" alt="css">
        </div>
    </section>
    <!--============= Header Section Ends Here =============-->
   <!--============= Contact Section Starts Here =============-->
    <section class="contact-section padding-top padding-bottom">
        <div class="container">
            <div class="section-header mw-100 cl-white">
                <h2 class="title">Fale Conosco</h2>
                <p>Se você está procurando uma demonstração, uma pergunta de suporte ou uma consulta comercial, entre em contato.</p>
            </div>
            <div class="row justify-content-center justify-content-lg-between">
                <div class="col-lg-7">
                    <div class="contact-wrapper">
                        <h4 class="title text-center mb-30">Entrar em Contato</h4>

                        <contact-submit></contact-submit>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="contact-content">
                        <div class="man d-lg-block d-none">
                            <img src="{{asset('assets/images/contact/man.png')}}" alt="bg">
                        </div>
                        <div class="section-header left-style">
                            <h3 class="title">Have questions?</h3>
                            <p>Have questions about payments or price
                                plans? We have answers!</p>
                            <a href="#0">Read F.A.Q <i class="fas fa-angle-right"></i></a>
                        </div>
                        <div class="contact-area">
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('assets/images/contact/contact1.png')}}" alt="contact">
                                </div>
                                <div class="contact-contact">
                                    <h5 class="subtitle">Email Us</h5>
                                    <a href="Mailto:info@mosto.com">info@mosto.com</a>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('assets/images/contact/contact2.png')}}" alt="contact">
                                </div>
                                <div class="contact-contact">
                                    <h5 class="subtitle">Call Us</h5>
                                    <a href="Tel:565656855">+1 (987) 664-32-11</a>
                                    <a href="Tel:565656855">+1 (987) 664-32-11</a>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('assets/images/contact/contact3.png')}}" alt="contact">
                                </div>
                                <div class="contact-contact">
                                    <h5 class="subtitle">Visit Us</h5>
                                    <p>4293 Euclid Avenue, Los Angeles,CA 90012</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('scripts')
    <script src="{{asset('assets/plugins/inputmask/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/inputmask/bindings/inputmask.binding.js')}}"></script>
    <script src="{{mix('assets/app/js/contact-app.js')}}"></script>
@endpush
