@extends('layouts.web')
@push('title')
    <title>Cadastrar Plano: {{session('plan')->name ?? ''}}</title>
@endpush
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/dark/css/main.css')}}">
@endpush
@section('content')

    <section id="appPlans" class="reliable-section padding-bottom padding-top bg_img pos-rel oh pb-xl-0 mt-120 mt-max-xl-0" data-background="{{asset('assets/images/bg/reliable-bg.jpg')}}" id="how">

        <div class="reliable-bottom d-xl-block d-none">
            <img src="{{asset('assets/dark/css/img/reliable-bottom.png')}}" alt="css">
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-xl-8">
                    <div class="section-header mw-100 white-xl-black">
                        <h2 class="title"> {{ 'Plano: '. session('plan')->name ?? ''}}</h2>
                        <p>Para realizar o cadastro preencha o formulário abaixo:</p>
                    </div>
                </div>
            </div>
            <plans-submit></plans-submit>
        </div>
    </section>

    <!--============= Sponsor Section Section Here =============-->
    <section class="sponsor-section padding-bottom">
        <div class="container">
            <div class="section-header mw-100">
                <h5 class="cate"></h5>
                <h2 class="title">Principais Clientes</h2>
                <div class="sponsor-slider-4 owl-theme owl-carousel">
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor1.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor2.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor3.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor4.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor5.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor6.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor7.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor1.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor2.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor3.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor4.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor5.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor6.png')}}" alt="sponsor">
                    </div>
                    <div class="sponsor-thumb">
                        <img src="{{asset('assets/images/sponsor/sponsor7.png')}}" alt="sponsor">
                    </div>
                </div>

            </div>
        </div>
    </section>

    {{--============= Como funciona: a seção começa aqui =============--}}
    {{--include('pages.includes._works_section')--}}
    {{--============= Como funciona: a seção termina aqui =============--}}


@endsection
@push('scripts')
    <script src="{{asset('assets/plugins/inputmask/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/inputmask/bindings/inputmask.binding.js')}}"></script>
    <script src="{{mix('assets/app/js/plans-app.js')}}"></script>
    {{--
    <script>
        $(".work-slider").owlCarousel({
            items: 1,
            margin: 60,
            nav: true,
            smartSpeed: 900,
            navText: ["Anterior","Próximo"]
        });
    </script>
    --}}
@endpush