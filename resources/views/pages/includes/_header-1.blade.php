<section class="banner-11 oh pos-rel">
    <div class="extra-bg bg_img" data-background="{{asset('assets/images/banner/banner11/banner11-bg.png')}}"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="banner-content-11">
                    <h1 class="title">Gerencie todo o seu trabalho em um só lugar</h1>
                    <p>Nosso aplicativo é simples, intuitivo e poderoso para gerenciar seu trabalho.</p>
                    @include('pages.includes._newsletter')
                </div>
                <div class="banner-odometer-two">
                    <div class="counter-item">
                        <div class="counter-thumb">
                            <img src="{{asset('assets/images/icon/counter1.png')}}" alt="icon">
                        </div>
                        <div class="counter-content">
                            <h2 class="title"><span class="counter">17501</span></h2>
                            <span>Usuários</span>
                        </div>
                    </div>
                    <div class="counter-item">
                        <div class="counter-thumb">
                            <img src="{{asset('assets/images/icon/counter2.png')}}" alt="icon">
                        </div>
                        <div class="counter-content">
                            <h2 class="title"><span class="counter">1987</span></h2>
                            <span>Visitantes</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 d-none d-lg-block">
                <div class="banner-thumb-11">
                    <div class="main-thumb wow slideInLeft">
                        <img src="{{asset('assets/images/banner/banner11/banner11.png')}}" alt="banner">
                    </div>
                    <div class="graph wow bounceInDown" data-wow-delay="3s" data-wow-duration="1s">
                        <img src="{{asset('assets/images/banner/banner11/graph.png')}}" alt="banner">
                    </div>
                    <div class="boy wow slideInRight" data-wow-delay="1s" data-wow-duration="1s">
                        <img src="{{asset('assets/images/banner/banner11/boy.png')}}" alt="banner">
                    </div>
                    <div class="girl wow slideInUp" data-wow-delay="1.5s" data-wow-duration="1s">
                        <img src="{{asset('assets/images/banner/banner11/girl.png')}}" alt="banner">
                    </div>
                    <div class="tree1 wow slideInUp" data-wow-delay="2s" data-wow-duration="1s">
                        <img src="{{asset('assets/images/banner/banner11/tree1.png')}}" alt="banner">
                    </div>
                    <div class="tree2 wow slideInUp" data-wow-delay="2.5s" data-wow-duration="1s">
                        <img src="{{asset('assets/images/banner/banner11/tree2.png')}}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>