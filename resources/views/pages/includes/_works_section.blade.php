<section class="work-section padding-bottom bg_img mb-md-95 pb-md-0" data-background="{{asset('assets/images/work/work-bg2.jpg')}}" id="how">
    <div class="oh padding-top pos-rel">
        <div class="top-shape d-none d-lg-block">
            <img src="{{asset('assets/dark/css/img/work-shape.png')}}" alt="Como funciona">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-7">
                    <div class="section-header left-style cl-white">
                        <h5 class="cate">Descreva seu aplicativo</h5>
                        <h2 class="title">Vamos ver como funciona</h2>
                        <p>É mais fácil do que você pensa. Siga as etapas simples e fáceis</p>
                    </div>
                </div>
            </div>
            <div class="work-slider owl-carousel owl-theme" data-slider-id="2">
                <div class="work-item">
                    <div class="work-thumb">
                        <img src="{{asset('assets/images/work/work1.png')}}" alt="work">
                    </div>
                    <div class="work-content cl-white">
                        <h3 class="title">Registro</h3>
                        <p>Primeiro, você precisa registrar uma conta de usuário em nosso site antes de configurá-lo e usá-lo regularmente</p>
                        <a href="#0" class="get-button white light">Iniciar <i class="flaticon-right"></i></a>
                    </div>
                </div>
                <div class="work-item">
                    <div class="work-thumb">
                        <img src="{{asset('assets/images/work/work1.png')}}" alt="work">
                    </div>
                    <div class="work-content cl-white">
                        <h3 class="title">Configurar</h3>
                        <p>Depois que realizou a conferência de seu cadastro, realize a configuração de seu sistema para utilização do APP de Vendas.</p>
                        <a href="#0" class="get-button white light">Get Started <i class="flaticon-right"></i></a>
                    </div>
                </div>
                <div class="work-item">
                    <div class="work-thumb">
                        <img src="{{asset('assets/images/work/work1.png')}}" alt="work">
                    </div>
                    <div class="work-content cl-white">
                        <h3 class="title">Integração</h3>
                        <p>Se você quiser integrar Facebook, Instagram e WhatsApp para potencializar as vendas, usando as estratégias certas, conte conosco! Nossa equipe está sempre preparada para te atender.</p>
                        <a href="#0" class="get-button white light">Get Started <i class="flaticon-right"></i></a>
                    </div>
                </div>
                <div class="work-item">
                    <div class="work-thumb">
                        <img src="{{asset('assets/images/work/work1.png')}}" alt="work">
                    </div>
                    <div class="work-content cl-white">
                        <h3 class="title">Yay! Done</h3>
                        <p>First, you need to register a user account on our website before
                            configuring and using it on a regular basis.</p>
                        <a href="#0" class="get-button white light">Get Started <i class="flaticon-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="count-slider owl-thumbs" data-slider-id="2">
            <div class="count-item">
                <span class="serial">01</span>
                <h5 class="title">Registro</h5>
            </div>
            <div class="count-item">
                <span class="serial">02</span>
                <h5 class="title">Configure</h5>
            </div>
            <div class="count-item">
                <span class="serial">03</span>
                <h5 class="title">Integração</h5>
            </div>
            <div class="count-item">
                <span class="serial">04</span>
                <h5 class="title">Pronto</h5>
            </div>
        </div>
    </div>
</section>
